#!/bin/sh

# Bash profile. Runs on login

export PATH='$PATH:$HOME/.bin'
export EDITOR='nvim'
export TERMINAL='alacritty'

